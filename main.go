package main

import (
	"gitlab.com/fredy.sosa/awesome-go/internal/transport"
	"gitlab.com/fredy.sosa/awesome-go/internal/usecase"
)

func main()  {
	uCase := usecase.NewUseCase()
	awesomeTransport := transport.NewAwesome(uCase)
	e := transport.NewAwesomeTransport(awesomeTransport)

	e.Logger.Fatal(e.Start(":8080"))
}