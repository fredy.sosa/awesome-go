package models

type ReversedRequest struct {
	Word string `json:"word"`
}

type ReversedResponse struct {
	Input    string `json:"input"`
	Reversed string `json:"reversed"`
}
