package transport

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/fredy.sosa/awesome-go/internal/models"
)

//go:generate mockgen -destination=./mocks/usecase_mock.go -package=mocks -source=awesome.go UseCase

type UseCase interface {
	IsAlive(testing bool) error
	Reverse(input string) (string, error)
}

type Awesome struct {
	UseCase UseCase
}

func NewAwesome(uc UseCase) Awesome {
	return Awesome{
		UseCase: uc,
	}
}

func (a Awesome) Ping(c echo.Context) error {
	if err := a.UseCase.IsAlive(false); err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, "pong")
}

func (a Awesome) Reversed(c echo.Context) error {
	body, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error()) // wrong err to return
	}
	var request models.ReversedRequest
	err = json.Unmarshal(body, &request)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	reversedWord, err := a.UseCase.Reverse(request.Word)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, err.Error())
	}
	response := models.ReversedResponse{
		Input:    request.Word,
		Reversed: reversedWord,
	}
	return c.JSON(http.StatusOK, response)
}
