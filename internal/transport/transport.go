package transport

import (
	"github.com/labstack/echo/v4"
)

type AwesomeTransport interface {
	Ping(c echo.Context) error
	Reversed(c echo.Context) error
}

func NewAwesomeTransport(at AwesomeTransport) *echo.Echo {
	e := echo.New()

	e.GET("/ping", at.Ping)
	e.POST("/reversed", at.Reversed)

	return e
}
