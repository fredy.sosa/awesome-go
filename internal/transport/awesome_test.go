package transport

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"gitlab.com/fredy.sosa/awesome-go/internal/models"

	"github.com/golang/mock/gomock"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"

	"gitlab.com/fredy.sosa/awesome-go/internal/transport/mocks"
)

var (
	genericErr = fmt.Errorf("some error")
)

func TestAwesome_Ping(t *testing.T) {
	type fields struct {
		UseCase func(m *mocks.MockUseCase)
	}
	type expectations struct {
		statusCode int
		body       string
	}
	tests := []struct {
		name         string
		fields       fields
		wantErr      bool
		expectations expectations
	}{
		{
			name: "fail: UseCase.IsAlive returns an error",
			fields: fields{
				func(m *mocks.MockUseCase) {
					m.EXPECT().IsAlive(false).
						Return(genericErr)
				},
			},
			wantErr: false,
			expectations: expectations{
				statusCode: http.StatusInternalServerError,
				body:       "\"some error\"\n",
			},
		},
		{
			name: "success: full right test",
			fields: fields{
				func(m *mocks.MockUseCase) {
					m.EXPECT().IsAlive(false).
						Return(nil)
				},
			},
			wantErr: false,
			expectations: expectations{
				statusCode: http.StatusOK,
				body:       "\"pong\"\n",
			},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			mockUseCase := mocks.NewMockUseCase(mockCtrl)
			if tt.fields.UseCase != nil {
				tt.fields.UseCase(mockUseCase)
			}
			a := Awesome{
				UseCase: mockUseCase,
			}
			e := echo.New()
			req := httptest.NewRequest(http.MethodGet, "/ping", nil)
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)
			if err := a.Ping(c); (err != nil) != tt.wantErr {
				t.Errorf("Ping() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, tt.expectations.statusCode, rec.Code)
			assert.Equal(t, tt.expectations.body, rec.Body.String())
		})
	}
}

func TestAwesome_Reversed(t *testing.T) {
	var (
		bodyToSendStruct = models.ReversedRequest{
			Word: "something",
		}
		bodyToSend, _ = json.Marshal(bodyToSendStruct)
	)
	type fields struct {
		UseCase func(m *mocks.MockUseCase)
		body    []byte
	}
	type expectations struct {
		statusCode int
		body       string
	}
	tests := []struct {
		name         string
		fields       fields
		expectations expectations
		wantErr      bool
	}{
		{
			name: "fail: cannot parse body",
			expectations: expectations{
				statusCode: http.StatusInternalServerError,
				body:       "\"unexpected end of JSON input\"\n",
			},
			wantErr: false,
		},
		{
			name: "fail: UseCase.Reverse returns an error",
			fields: fields{
				UseCase: func(m *mocks.MockUseCase) {
					m.EXPECT().Reverse("something").
						Return("", genericErr)
				},
				body: bodyToSend,
			},
			expectations: expectations{
				statusCode: http.StatusInternalServerError,
				body:       "\"some error\"\n",
			},
			wantErr: false,
		},
		{
			name: "success: full right test",
			fields: fields{
				UseCase: func(m *mocks.MockUseCase) {
					m.EXPECT().Reverse("something").
						Return("gnihtemos", nil)
				},
				body: bodyToSend,
			},
			expectations: expectations{
				statusCode: http.StatusOK,
				body:       "{\"input\":\"something\",\"reversed\":\"gnihtemos\"}\n",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			mockCtrl := gomock.NewController(t)
			defer mockCtrl.Finish()

			mockUseCase := mocks.NewMockUseCase(mockCtrl)
			if tt.fields.UseCase != nil {
				tt.fields.UseCase(mockUseCase)
			}
			a := Awesome{
				UseCase: mockUseCase,
			}
			e := echo.New()
			var body io.Reader
			if tt.fields.body != nil {
				body = bytes.NewReader(tt.fields.body)
			}
			req := httptest.NewRequest(http.MethodPost, "/reversed", body)
			rec := httptest.NewRecorder()
			c := e.NewContext(req, rec)
			if err := a.Reversed(c); (err != nil) != tt.wantErr {
				t.Errorf("Reversed() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, tt.expectations.statusCode, rec.Code)
			assert.Equal(t, tt.expectations.body, rec.Body.String())
		})
	}
}

func TestNewAwesome(t *testing.T) {
	type args struct {
		uc UseCase
	}
	tests := []struct {
		name string
		args args
		want Awesome
	}{
		{
			name: "success: full right test",
			args: args{
				uc: &mocks.MockUseCase{},
			},
			want: Awesome{
				UseCase: &mocks.MockUseCase{},
			},
		},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			if got := NewAwesome(tt.args.uc); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewAwesome() = %v, want %v", got, tt.want)
			}
		})
	}
}
