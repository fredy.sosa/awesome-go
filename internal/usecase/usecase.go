package usecase

import (
	"strings"

	"gitlab.com/fredy.sosa/awesome-go/internal/constants"
)

type UseCase struct {
}

func NewUseCase() UseCase{
	return UseCase{}
}

func (u UseCase) IsAlive(testing bool) error {
	if testing {
		return constants.ErrNotAlive
	}
	return nil
}

func (u UseCase) Reverse(input string) (string, error) {
	if strings.EqualFold(input, "hello world") {
		return "", constants.ErrAnotherHelloWord
	}
	return Reverse(input), nil
}

func Reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}
