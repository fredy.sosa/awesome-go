package constants

import "fmt"

var (
	ErrNotAlive = fmt.Errorf("the service is not alive")
	ErrAnotherHelloWord = fmt.Errorf("the input is a hello world again")
)
